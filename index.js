import express, { json } from "express";
import firstRouter from "./src/routes/firstRouter.js";
import foodRouter from "./src/routes/foodRouter.js";
import mongoose from "mongoose";
import connectDb from "./src/connectdb/connectMongodb.js";
import teacherRouter from "./src/routes/teacherRouter.js";
import clothRouter from "./src/routes/clothRouter.js";
import randomRouter from "./src/routes/randomRouter.js";
import userRouter from "./src/routes/userRouter.js";
import productRouter from "./src/routes/productRouter.js";
import reviewRouter from "./src/routes/reviewRouter.js";
import jwt from "jsonwebtoken";
import { sendMail } from "./src/utils/sendmail.js";
import webUserRouter from "./src/routes/webUserRouter.js";
//make express application
let expressApp = express();
expressApp.use(json());

connectDb();

// expressApp.use((req, res, next) => {
//   console.log("i am application middleware1");
//   next();
// });

// attached port to the express application instance

expressApp.listen(8000, () => {
  console.log("app is listening at port 8000");
});

expressApp.use("/", firstRouter);
expressApp.use("/food", foodRouter); //
expressApp.use("/teachers", teacherRouter); //
expressApp.use("/cloths", clothRouter); //
expressApp.use("/randoms", randomRouter); //
expressApp.use("/users", userRouter); //
expressApp.use("/products", productRouter); //
expressApp.use("/reviews", reviewRouter); //
expressApp.use("/web-users", webUserRouter); //

// expressApp.use((req, res, next) => {
//   console.log("i am application middleware2");
// });

// generate token
// let infoObj = {
//   name: "nitan",
//   age: 29,
//   _id: "12341234123",
// };

// let secretKey = "express5";

// let expiryInfo = {
//   expiresIn: "365d",
// };

// let token = jwt.sign(infoObj, secretKey, expiryInfo);
// console.log(token);

//***verify token */
// let token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibml0YW4iLCJhZ2UiOjI5LCJfaWQiOiIxMjM0MTIzNDEyMyIsImlhdCI6MTY5MzcwNjM4OSwiZXhwIjoxNzI1MjQyMzg5fQ.KIpDJM0XsrIyKJzVEP67SvcY8Sfqglqcbeor1sr9OXI";
// let infoObj = jwt.verify(token, "express6");

// console.log(infoObj);

//verify
// the token should not exceed its expiry date
// if token is made from give secretkey  then the token will be verify else it is not

//if verify
// infoObje

// if not veirify
//error

//send mail

// await sendMail({
//   from: '"Houseofjob" <uniquekc425@gmail.com>"',
//   to: ["nitanthapa425@gmail.com", "nagendrabsthapa7351@gmail.com"],
//   subject: "account create",
//   html: `

//   `,
// });
