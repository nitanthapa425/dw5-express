import { Random } from "../schema/model.js";

export const createRandomController = async (req, res) => {
  let data = req.body;

  try {
    let result = await Random.create(data);
    res.status(201).json({
      success: true,
      message: "Random created successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllRandomController = async (req, res) => {
  try {
    // let result = await Random.find({});
    // let result = await Random.find({ name: "nitan" });
    //********************in searching type does not matter (only value matter)
    // let result = await Random.find({ age: "29" });
    //*******************in find method output will be either empty array or arrray of object */

    // ************************ if you try to search the filed which is not in database (schema) it will results empty array
    // let result = await Random.find({ homeasdfasdf: 100 });
    // let result = await Random.find({ name: "nitan", age: 29 });

    // for number case
    // let result = await Random.find({age:29})
    // let result = await Random.find({ age: "29" });
    // let result = await Random.find({ age: { $gt: 25 } });
    // let result = await Random.find({ age: { $gte: 25 } });
    // let result = await Random.find({ age: { $lt: 25 } });
    // let result = await Random.find({ age: { $lte: 25 } });
    // let result = await Random.find({ age: { $ne:25} });
    //age 26,29
    // let result = await Random.find({ age: { $in: [26, 29,16] } });
    // and , or

    // let result = await Random.find({$or:[
    //   {age:29},
    //   {name:"chimmi"},
    //   {isMarried:true}

    // ]})
    // let result = await Random.find({
    //   $and: [{ age: 29 }, { name: "chimmi" }, { isMarried: true }],
    // });

    // fro range

    //20,21,22,23,24,25

    // let result = await Random.find({
    //   $and: [{ age: { $gte: 20 } }, { age: { $lte: 25 } }],
    // });

    // let result = await Random.find({
    //   name: { $nin: ["nitan"] },
    // });

    //find({name:"nitan"})
    // find({ "location.country": "nepal" });
    // find({favTeacher:"bhishma"})

    // object search
    // let result = await Random.find({
    //   "location.country": "nepal",
    // });

    // array search
    //   let result = await Random.find({
    //  favTeacher:"bhishma"
    // });

    //array of objects
    // let result = await Random.find({
    //   "favSubject.bookAuthor": "nitan",
    // });

    //for select

    // let result = await Random.find({}).select("name age -_id");
    // let result = await Random.find({}).select("-name -age"); // it will throw error
    // let result = await Random.find({}).select("-name -age password");

    //sort

    res.status(200).json({
      success: true,
      message: "Random read successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readRandomByIdController = async (req, res) => {
  // console.log(req.params);
  let randomId = req.params.randomId;

  try {
    let result = await Random.findById(randomId);

    res.status(200).json({
      success: true,
      message: "Random read successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export const updateRandomByIdController = async (req, res) => {
  let randomId = req.params.randomId;
  let data = req.body;

  try {
    let result = await Random.findByIdAndUpdate(randomId, data, {
      new: true,
    });
    res.status(201).json({
      success: true,
      message: "Random updated successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteRandomByIdController = async (req, res) => {
  let randomId = req.params.randomId;

  try {
    let result = await Random.findByIdAndDelete(randomId);
    res.status(200).json({
      success: true,
      message: "Random deleted successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
