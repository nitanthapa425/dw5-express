import bcrypt from "bcrypt";
import { WebUser } from "../schema/model.js";
import { secretKey } from "../constant/constant.js";
import jwt from "jsonwebtoken";
import { sendMail } from "../utils/sendmail.js";

// let hashedPassword = await bcrypt.hash(req.body.password, 10);
export const createWebUserController = async (req, res) => {
  try {
    let data = req.body;

    let hashPassword = await bcrypt.hash(data.password, 10);

    data = {
      ...data,
      password: hashPassword,
      isVerifiedEmail: false,
    };

    let result = await WebUser.create(data);

    // generate token
    let infoObj = {
      _id: result._id,
    };

    let expiryInfo = {
      expiresIn: "5m",
    };

    let token = jwt.sign(infoObj, secretKey, expiryInfo);

    await sendMail({
      from: '"Express5" <uniquekc425@gmail.com>"',
      to: [data.email],
      subject: "account create",
      html: `
          <h1> your account has been created successfully please click the given link to verify your account </h1>
          <a href="http://localhost:3000/verifyAccount?token =${token}">http://localhost:3000/verifyAccount?token =${token}</a>
          `,
    });
    res.status(201).json({
      success: true,
      message: "user created successfully.",
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const verifyEmailController = async (req, res) => {
  //
  //postman => token
  // get token from postman
  // verify token
  //_id
  //update data of _id
  // update with isVefiedEmail:true

  //get token

  try {
    let tokenString = req.headers.authorization;
    let tokenArray = tokenString.split(" ");
    let token = tokenArray[1];

    //verify token
    let infoObj = jwt.verify(token, secretKey);

    let _id = infoObj._id;

    let result = await WebUser.findByIdAndUpdate(
      _id,
      {
        isVerifiedEmail: true,
      },
      {
        new: true,
      }
    );

    res.status(200).json({
      success: true,
      message: "Email verified successfully",
    });
  } catch (error) {
    res.status(401).json({
      success: false,
      message: error.message,
    });
  }
};

export const loginUserController = async (req, res) => {
  try {
    let email = req.body.email;
    let password = req.body.password;

    //is email register

    let user = await WebUser.findOne({ email: email });
    console.log(user);

    let isVerifiedEmail = user.isVerifiedEmail;

    if (user) {
      if (isVerifiedEmail) {
        let isValidPassword = await bcrypt.compare(password, user.password);

        if (isValidPassword) {
          let infoObj = {
            _id: user._id,
          };
          let expiryInfo = {
            expiresIn: "365d",
          };
          let token = jwt.sign(infoObj, secretKey, expiryInfo);

          res.status(200).json({
            success: true,
            message: "Login successfully",
            data: token,
          });
        } else {
          let error = new Error("credential does not match");
          throw error;
        }
      } else {
        let error = new Error("credential does not match");
        throw error;
      }
    } else {
      let error = new Error("credential does not match");
      throw error;
    }
  } catch (error) {
    res.status(401).json({
      success: false,
      message: error.message,
    });
  }
};

//status code
//success
//200 get , delete
//201 create , updated
//error
// 400
//401 => token not valid

export const readAllWebUserController = async (req, res) => {
  try {
    let result = await WebUser.find({});
    res.status(200).json({
      success: true,
      message: "WebUser read successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export const readWebUserByIdController = async (req, res) => {
  // console.log(req.params);
  let webUserId = req.params.webUserId;

  try {
    let result = await WebUser.findById(webUserId);

    res.status(200).json({
      success: true,
      message: "WebUser read successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export const updateWebUserByIdController = async (req, res) => {
  let webUserId = req.params.webUserId;
  let data = req.body;

  try {
    let result = await WebUser.findByIdAndUpdate(webUserId, data, {
      new: true,
    });
    res.status(201).json({
      success: true,
      message: "WebUser updated successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteWebUserByIdController = async (req, res) => {
  let webUserId = req.params.webUserId;

  try {
    let result = await WebUser.findByIdAndDelete(webUserId);
    res.status(200).json({
      success: true,
      message: "WebUser deleted successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

// in bcrypt package

// let hashedPassword = await bcrypt.hash(password, 10) for hashing
//                                            password,   hashedPassword
// let isValidPassoed=  await bcrypt.compare("Password@1","$2b$10$pbDKu2eQkyzPWnHLIZbWS.lgW/YweaSJA./vy51vbcd7p5VhhduK2")
// this code will return either true or false
// it return true if hashPassword is made from password else it return false
