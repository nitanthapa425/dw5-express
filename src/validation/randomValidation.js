import Joi from "joi";

let randomValidation = Joi.object()
  .keys({
    name: Joi.string()
      .required()
      .min(3)
      .max(10)
      .message({
        "any.required": "name is required",
        "string.base": "field must be string",
        "string.min": "name must be at least 3 character",
        "string.max": "name must be at most 10 character",
      })
      .allow(""),

    age: Joi.number()
      .required()
      // .min(18)
      // .max(60),
      .custom((value, msg) => {
        if (value >= 18) {
          return true;
        } else {
          return msg.message("age must be at least 18");
        }
      })
      .message({
        "any.required": "age is required",
        "number.base": "field must be string",
      }),
    isMarried: Joi.boolean().required(),
    spouseName: Joi.when("isMarried", {
      is: true,
      then: Joi.string().required(),
      otherwise: Joi.string(),
    }),

    //  if married= true  => spouseNmae => required
    // if married= flase => sposeName is not required
    gender: Joi.string().required().valid("male", "female", "other").messages({
      "string.base": "gender must be string",
      "any.required": "gender is required",
      "any.only": "gender must be either male or female or other",
    }),
    email: Joi.string()
      .required()
      .custom((value, msg) => {
        let validEmail = value.match(
          /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b/
        );
        if (validEmail) {
          return true;
        } else {
          return msg.message("email is not valid");
        }
      }),

    password: Joi.string()
      .required()
      .custom((value, msg) => {
        let isValidPassword = value.match(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()-_=+{};:,<.>/?]).{8,15}$/
        );

        if (isValidPassword) {
          return true;
        } else {
          return msg.message(
            "password must have at least one uppercase, one lowercase , one number ,one symbol, min 8 character and max 15 character"
          );
        }
      }),

    //min => 10
    //max=>10

    phoneNumber: Joi.number()
      .required()
      .custom((value, msg) => {
        let phoneNumberString = String(value);
        let phoneNumberLength = phoneNumberString.length;
        if (phoneNumberLength === 10) {
          return true;
        } else {
          return msg.message("phone number must be exact 10 character ");
        }
      }),

    roll: Joi.number().required(),
    dob: Joi.string().required(),

    location: Joi.object().keys({
      country: Joi.string().required(),
      exactLocation: Joi.string().required(),
    }),

    favTeacher: Joi.array().items(Joi.string().required()),

    favSubject: Joi.array().items(
      Joi.object().keys({
        bookName: Joi.string().required(),
        bookAuthor: Joi.string().required(),
      })
    ),
  })
  .unknown(true);

export default randomValidation;
