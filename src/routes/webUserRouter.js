//define router
import { Router } from "express";
import {
  createWebUserController,
  deleteWebUserByIdController,
  loginUserController,
  readAllWebUserController,
  readWebUserByIdController,
  updateWebUserByIdController,
  verifyEmailController,
} from "../controller/webUserController.js";

let webUserRouter = Router();

webUserRouter.route("/").post(createWebUserController);
// .get(readAllWebUserController);

webUserRouter.route("/verify-email").post(verifyEmailController);

webUserRouter.route("/login").post(loginUserController);
webUserRouter.route("/:webWebUserId");
// .get(readWebUserByIdController)
// .patch(updateWebUserByIdController)
// .delete(deleteWebUserByIdController);

export default webUserRouter;
