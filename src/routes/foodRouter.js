import { Router } from "express";
let foodRouter = Router();

//query

// {{url}}/food?name=nitan&age=29
//get

foodRouter
  .route("/") //localhost:8000/food
  .post((req, res) => {
    console.log(req.body);
    res.json({
      success: true,
      message: "food created successfully.",
    });
  })
  .get((req, res) => {
    console.log(req.query)
    res.json({
      success: true,
      message: "food read successfully.",
    });
  })
  .patch((req, res) => {
    res.json({
      success: true,
      message: "food  updated successfully.",
    });
  })
  .delete((req, res) => {
    res.json({
      success: true,
      message: "food deleted  successfully.",
    });
  });

//localhost:8000/food/any/ram/any

foodRouter
  .route("/:id") //localhost:8000/food/any
  .post((req, res) => {
    console.log(req.params);

    res.json("hello");
  });

foodRouter.route("/:id1/ram/:id2").post((req, res) => {
  console.log(req.params);

  res.json("hello");
});

export default foodRouter;
