//define router
import { Router } from "express";
import {
  createTeacherController,
  deleteTeacherByIdController,
  readAllTeacherController,
  readTeacherByIdController,
  updateTeacherByIdController,
} from "../controller/teacherControler.js";
import validation from "../middleware/validation.js";
import teacherValidation from "../validation/teacherValidation.js";
let teacherRouter = Router();

// teachers/1234234234
//Teacher.create(data)
//Teacher.find({})
//Teacher.findById(id)
//Teacher.findByIdAndDelete(id)
//Teacher.findByIdAndUpdated(id,data)

teacherRouter
  .route("/") //localhost:8000/teachers
  .post(validation(teacherValidation), createTeacherController)
  .get(readAllTeacherController);

teacherRouter
  .route("/:teacherId")
  .get(readTeacherByIdController)
  .patch(updateTeacherByIdController)
  .delete(deleteTeacherByIdController);

export default teacherRouter;

//status code

//success  2XX
//create  ======== 201
//read ======200
//update ======= 201
//delete ======200

//error 4XX
//404 for api not found or resource not found
//400 //bad request

// {
// "name":"bhishmasirjihhhha",
// "age":29
// "isMarried": false,
// "subject": "math",
// }
