//define router
import { Router } from "express";
import {
  createRandomController,
  deleteRandomByIdController,
  readAllRandomController,
  readRandomByIdController,
  updateRandomByIdController,
} from "../controller/randomController.js";
import Joi from "joi";
import validation from "../middleware/validation.js";
import randomValidation from "../validation/randomValidation.js";

let randomRouter = Router();

// middleware => fun => req,res,next

// let m0 = (value) => {
//   return (req, res, next) => {
//     console.log("i am middleware 0");
//     if (value) {
//       next();
//     } else {
//     }
//   };
// };

// let m1 = (req, res, next) => {
//   console.log("i am m1 middleware");
// };

//without function call (use if you dont want to pass value) (req,res,next)=> {}
// with function call (use if you wat to pass value)  ()=>{ return((req,res,next)=> {})}

// let deleteUser = (req, res, next) => {
//   console.log("user deleted successfully.");
// };

// let isAuthorized = (value) => {
//   return (req, res, next) => {
//     //if admin or superadmin => delete
//     //else => cant delete

//     if (value === "admin" || value === "superadmin") {
//       next();
//     } else {
//       console.log("you can not delete user");
//     }
//   };
// };

//

// .string()
// value must be string
// it should not be empty
//.min(3) => the field must have at least 3 character
//.max(10)=> the field must have at most 10 character

// .number()
//value muse be number (it does not see type for number)
//it means 21 and "21" are same
//.min()
//.max()

//.boolean()
//value must be boolean

//required  => any (string, number, boolean)
// you must pass field

//enum => fixed value (male, female,other)
// .valid("male","female","other")

//through custum error

//object

// Joi.object().keys({
//   ....
// })

//array
// Joi.array().items()

//.when()

//.string()
// must be string
// does not allow ""

randomRouter
  .route("/") //localhost:8000/randoms
  // .post(m0(false), m1)
  // .post(isAuthorized("custumer"), deleteUser)
  .post(validation(randomValidation), createRandomController)
  .get(readAllRandomController);

randomRouter
  .route("/:randomId")
  .get(readRandomByIdController)
  .patch(updateRandomByIdController)
  .delete(deleteRandomByIdController);

export default randomRouter;
