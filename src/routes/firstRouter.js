//define router
import { Router } from "express";
let firstRouter = Router();

//middleware
//normal middleware (req,res,next)=> {}  => to trigger we have call next()
//error middleware  (err,req,res,next)=>{} =>  to trigger we have to call next(variable) //or it will occur if error is occurred
// a normal middleware can trigger normal middleware as well as error middleware
// a error middleware can trigger normal middleware as will as error middleware

//each api must have response
// and api must send only one request

// we have two middleware
// route middleware
// application middleware
//when you hit api first a program will start form index and ends with index

firstRouter
  .route("/") //localhost:8000
  .post(
    (req, res, next) => {
      //m1=> normal
      console.log("i am middleware1");
      res.json("aaaa");
      // res.json("i am response and will appeared at postman");
      next("a");
    },
    (err, req, res, next) => {
      //m2 =>error
      console.log("i am middleware2");

      next("b");
    },
    (err, req, res, next) => {
      //m3 => error
      console.log("i am middleware3");
      next();
    },
    (req, res, next) => {
      //m4 => normal
      console.log("i am middleware4");
    }
  )
  .get((req, res, next) => {
    res.json("home get");
    next();
  })
  .patch((req, res) => {
    res.json("home patch");
  })
  .delete((req, res) => {
    res.json("home delete");
  });

firstRouter
  .route("/name") //localhost:8000/name
  .post((req, res) => {
    res.json("name post");
  })
  .get((req, res) => {
    res.json("name get");
  })
  .patch((req, res) => {
    res.json("name patch");
  })
  .delete((req, res) => {
    res.json("name delete");
  });

export default firstRouter;
