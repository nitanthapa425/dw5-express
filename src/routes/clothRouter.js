import { Router } from "express";
import {
  createClothController,
  deleteClothByIdController,
  readAllClothController,
  readClothByIdController,
  updateClothByIdController,
} from "../controller/clothController.js";
let clothRouter = Router();

clothRouter
  .route("/") //localhost:8000/cloths
  .post(createClothController)
  .get(readAllClothController);

clothRouter
  .route("/:clothId")
  .get(readClothByIdController)
  .patch(updateClothByIdController)
  .delete(deleteClothByIdController);

export default clothRouter;
