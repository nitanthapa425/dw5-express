//define router
import { Router } from "express";
import {
  createProductController,
  deleteProductByIdController,
  readAllProductController,
  readProductByIdController,
  updateProductByIdController,
} from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/")
  .post(createProductController)
  .get(readAllProductController);

productRouter
  .route("/:productId")
  .get(readProductByIdController)
  .patch(updateProductByIdController)
  .delete(deleteProductByIdController);

export default productRouter;
