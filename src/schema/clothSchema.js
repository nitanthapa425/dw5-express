import { Schema } from "mongoose";

let clothSchema = Schema({
  size: {
    type: String,
    required: [true, "size field is required."],
  },
  price: {
    type: Number,
    required: [true, "price field is required."],
  },
  color: {
    type: String,
    required: [true, "color field is required"],
  },
  brand: {
    type: String,
    required: [true, "brand field is required"],
  },
  category: {
    type: String,
    required: [true, "brand field is required"],
  },
  quantity: {
    type: Number,
    required: [true, "brand field is required"],
  },
  isAvailable: {
    type: Boolean,
    required: [true, "isAvailable field is required"],
  },
});

export default clothSchema;
