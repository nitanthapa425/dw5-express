//name

import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import teacherSchema from "./teacherScheam.js";
import clothSchema from "./clothSchema.js";
import randomSchema from "./randomSchema.js";
import userSchema from "./userSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";
import webUserSchema from "./webUser.js";

//Schema
// array name must be same as model name
// array bane must be singular and first letter capital
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Cloth = model("Cloth", clothSchema);
export let Random = model("Random", randomSchema);
export let User = model("User", userSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);
export let WebUser = model("WebUser", webUserSchema);
