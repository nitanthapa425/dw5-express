import { Schema } from "mongoose";

let randomSchema = Schema({
  name: {
    type: String,
    //data manipulation
    // uppercase: true,
    // lowercase:true,
    // trim: true,
    // default: "shyam",

    //validation********************************

    // required: [true, "name field is required"],
    //minLength
    //maxLength
    // minLength: [3, "name field must be at least 3 character long"],
    // maxLength: [10, "name filed must be at most 10 character"],
  },
  age: {
    type: Number,
    // required: "age field is required",
    //min
    //max
    // min: [18, "age field must be at least 18"],
    // max: [28, "ag field must be at most 28"],
  },
  password: {
    type: String,
    // required: "password field is required",
  },
  phoneNumber: {
    type: Number,
    // required: "phoneNumber field is required",
  },
  roll: {
    type: Number,
    // required: "roll field is required",
  },
  spouseName: {
    type: String,
    // required: "spouseName field is required",
  },
  email: {
    type: String,
    // required: "email field is required",
  },
  gender: {
    type: String,
    // required: "gender field is required",
    // enum: {
    //   values: ["male", "female", "other"],
    //   message: (notEnum) => {
    //     return `${notEnum.value} is not valid enum`;
    //   },
    // },
  },
  dob: {
    type: Date,
    // required: "dob field is required",
  },
  location: {
    country: {
      type: String,
      // required: "country field is required",
    },
    exactLocation: {
      type: String,
      // required: "exactLocation field is required",
    },
  },
  favTeacher: [
    {
      type: String,
      // required: "favTeacher field is required",
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
        // required: "bookName field is required",
      },
      bookAuthor: {
        type: String,
        // required: "bookAuthor field is required",
      },
    },
  ],
});

export default randomSchema;

//phone number ( 10 character exact)

//Number

//2character
// 10
// 99

//3 character
// 100
// 999

//10 character
// 1000000000;
// 9999999999;
