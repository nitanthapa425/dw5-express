const { string } = require("joi")

const { object } = require("joi")

// for normal searching
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]


//for  regex searchin
[
   {name:"nit",age:29, isMarried:false},
    {name:"sandinip",age:25, isMarried:false},
    {name:"ni",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    *{name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    *{name:"narendran",age:27, isMarried:false},
    *{name:"Nitan",age:16, isMarried:false},
    {name:"nitanthapa",age:22, isMarried:false},
]


find({name:"nitan"})//exact searching
find({name:/nitan/})// regex searching => not exact searching
find(name:/nitan/i)
find(name:/ni/)
find(name:/^ni/)
find(name:/^ni/i)
find(name:/n$/i)

find(name:/^(?=.*@)(?=.*_)|(?=.*_)(?=.*@)/)





//for array and object searching
[
    //searching
    //search according field
  
    {
      name: "nitan",
      location: {
        country: "nepal",
        exactLocation: "gagal",
      },
      favTeacher: ["bhishma", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "ram",
      location: {
        country: "china",
        exactLocation: "sikhu",
      },
      favTeacher: ["hari", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "shyam",
      location: {
        country: "india",
        exactLocation: "bihar",
      },
      favTeacher: ["niti", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
  ];
  
  //find({name:"nitan"})
  // find({ "location.country": "nepal" });
  // find({favTeacher:"bhishma"})
  //find({"favSubject.bookAuthor":"nitan"})
  

  //find it determine which object to show or not
  //select
  //sort
  //limit
  //skip


  //for sorting
  [
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"c",age:40, isMarried:false},
  
]
//output
[
  {name:"ab",age:60, isMarried:false},
  {name:"ab",age:50, isMarried:false},
  {name:"ac",age:29, isMarried:false},
  {name:"b",age:40, isMarried:false},
  {name:"c",age:40, isMarried:false},




  





  

 

]


//number sorting work properly unlike javascript
// find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")

// //ascending sort  descending sort
// 

// find({}).sort("-name age")

// find({}).sort("age -name")


//skip
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},

]

find({}).skip("8")



//limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//outptu
[
  {name:"nitan",age:29, isMarried:false},
  {name:"sandip",age:25, isMarried:false},

]

find({}).limit("2")




//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[
  {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
 
 


]



find({}).limit("5").skip("2")

//this order works
//find , sort, select, skipt, limit





///in mongodb
//array => collection
//object  => document

//collection of document

//find
//select 
//sort
//skip 
//limit

// note firs find works then select works then sort then skip and limit
// don't use limit like limit(2) use in the form limit("2")
// don't use skip like skip(2) use in the form skip("2")




//joi validataion



// schema
// model
// controller
// route
//index


//learn topic
// hashing
// token

//hashing => encryption
// it is use to secure password

// Password@1234 ========= asdkfj123423*alksdfjsalkdfjalsdf1234
// // bcryptjs => pacakge


// bcrypt.hash(password,10)






//generate id card => generate token
// verify id cared => verify token

//details => obj
//logo =>
//expiry data



//token************************************************
//mail send



//token

//generate token
// infoObj => any but better to use object
// secretKey => any string
// expiryInfo=>{
//   expiresIn:"366d"
// }

// jwt.sign(infoObj,secretKey,expiryInfo)

// //verify tokein
// jwt.verify(token, secretKey)





// jwt => package
    // npm i jsonwebtoken
// json web token
// generate token
// verify token 



//send mail

// gmail login 
// email 
// password


//mail send
// to information
// subject 


//send mail
// nodemailer npm i nodemailer




//login management



//1)Register process
//Register
//verify email

//2)Login


//3)My Profile


//4)My Profile Update
//we do not updated email and password


//5)Update password


//6 Forgot and reset password



//Register
//fullName
//email
//password
//dob
//gender
//role 
  // =>admin => product crud
  // =>superAdmin => he can delete admin
  // => customer => he can order buy product
//isVerified




//Register process
// Register
// verify email
  //  isVerifiedEmail:true



//login
// email and password
// check if email exist  (if not throw error)
// chedk if email verified (if not throw error)
// check if password match (if note throw error)
//generate token => _id
//send that token



//my-profile






